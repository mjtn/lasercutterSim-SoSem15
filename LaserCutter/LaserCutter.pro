#-------------------------------------------------
#
# Project created by QtCreator 2015-05-23T16:51:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LaserCutter
TEMPLATE = app

#LIBS += -pthreads
#QMAKE_CXXFLAGS += -lpthreads

SOURCES += main.cpp\
        lasercuttergui.cpp \
    drawop.cpp \
    Parser.cpp \
    LaserState.cpp \
    LaserOn.cpp \
    LaserOff.cpp \
    LaserCutter.cpp \
    LaserLaser.cpp \
    LaserError.cpp

ICON = laserbeam.icns

HEADERS  += lasercuttergui.h \
    drawop.h \
    Parser.h \
    LaserState.h \
    LaserOn.h \
    LaserOff.h \
    LaserCutter.h \
    LaserLaser.h \
    LaserError.h

FORMS    += lasercuttergui.ui

CONFIG += c++11
