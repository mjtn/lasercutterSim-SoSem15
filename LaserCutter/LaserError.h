#ifndef LASERERROR_H
#define LASERERROR_H

#include "LaserCutter.h"
#include "LaserState.h"

class LaserError : public LaserState
{
public:
    LaserError();
    ~LaserError();

    void off(LaserCutter*);
    void error(LaserCutter*);
};

#endif // LASERERROR_H
