#include "LaserError.h"
#include "LaserOff.h"

LaserError::LaserError()
{

}
LaserError::~LaserError()
{

}

void LaserError::off(LaserCutter* lsr)
{
    lsr->setCurrentState(new LaserOff);
    lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::OFF);
    delete this;
}

void LaserError::error(LaserCutter*)
{

}
