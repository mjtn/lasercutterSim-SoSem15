#ifndef LASERSTATE_H
#define LASERSTATE_H
#include "LaserCutter.h"

#include <iostream>
#include "lasercuttergui.h"

using std::cout;
using std::endl;

/* Basisklasse der State Machine */
class LaserState
{
public:
    // Konstruktor
    LaserState();
    virtual ~LaserState() = 0;
        // Pure virtual Function um die Klasse abstract zu machen
    // Methoden der jeweiligen States

    virtual void on(LaserCutter*)
    {
        cout << "BasisKlasse: on()" << endl;
    }

    virtual void off(LaserCutter*)
    {
       cout << "BasisKlasse: off()" << endl;

    }
    virtual void move(LaserCutter*, vector<int>)
    {
        cout << "BasisKlasse: move()" << endl;
    }
    virtual void error(LaserCutter* lsr)
    {
        lsr->abortDrawing();

        lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::ERROR);
        cout << "BasisKlasse: error()" << endl;
    }

};

#endif // LASERSTATE_H
