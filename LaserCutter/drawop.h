#ifndef DRAWOP_H
#define DRAWOP_H

#include <QWidget>
#include <QPainter>

using std::vector;
typedef vector<int> Column;

class DrawOP : public QWidget
{
    Q_OBJECT
public:
    explicit DrawOP(QWidget *parent = 0);

    /* Methoden */
    void setJumpIndex();
    void resetDrawPlate();
    void addCoordinatesToVector(vector<int>);

signals:

public slots:

protected:
     void paintEvent(QPaintEvent* e);

private:
    /* Variablen */
    vector< vector<int>> coordinates;
    vector<int> indexStateOff;

    /* Methoden */
    bool jumpOfStateOff(int *);
};

#endif // DRAWOP_H
