#ifndef LASERCUTTERGUI_H
#define LASERCUTTERGUI_H

#include <QMainWindow>
#include "LaserCutter.h"
#include "ui_lasercuttergui.h"


namespace Ui {
class LaserCutterGUI;
}

class LaserCutterGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit LaserCutterGUI(QWidget *parent = 0);
    ~LaserCutterGUI();

    /* Methoden fuer die Kommunikation des Controllers mit der UI */
    void addCoordinatesToDrawPlate(std::vector<int>);
    void setLaserCutterStateOnUI(std::string, Qt::GlobalColor);
    void toggleBtnStatus(bool);
    void setIndexOfStateOff();
    void msgBoxShowWithErrorNotification(std::string*);


    /* Erstellte Methoden von QT (Events) */
private slots:
    void on_btnPath_clicked();

    void on_btnStart_clicked();

    void on_btnClose_clicked();

    void on_btnStop_clicked();

private:
    Ui::LaserCutterGUI* ui;
    std::vector<int> tmp;
    LaserCutter* lsrCutter;
    void resetLsrCutter();

};

#endif // LASERCUTTERGUI_H
