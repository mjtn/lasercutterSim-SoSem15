#pragma once
#include <string>
#include <fstream>
#include <vector>
#include <iostream>

using std::string;
using std::vector;

/* Model Klasse */

class Parser
{
public:

    /* Variablen */
    vector <string> opCodes;

    /* Konstr,- Destruktor */
    Parser(string, class LaserCutter*);
    ~Parser();

    /* Methoden */
    void readOPCodeFromFile();
    void getCoordinates(int posInVec, int* x, int* y);
    void switchStates(string*, int x, int y);

private:
    /* Referenz in den Controller */
    class LaserCutter* lsr = NULL;

    /* Variablen */
    std::ifstream fi;
    string path;
    vector <string> dispatchTable;

    /* Methoden */
    void initDispatch();
    bool validOPCode(string*);

};
