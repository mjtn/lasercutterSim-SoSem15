#include "LaserOff.h"
#include "LaserState.h"
#include "LaserOn.h"
#include "lasercuttergui.h"
#include "ui_lasercuttergui.h"

LaserOff::LaserOff()
{

}

LaserOff::~LaserOff()
{

}

void LaserOff::on(LaserCutter* lsr)
{
    cout << "Laser wird umgeschaltet in LaserOff=>NewState = On()\n";
    lsr->setCurrentState(new LaserOn);

    lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::ON);

    delete this;
}

void LaserOff::move(LaserCutter* lsr, vector<int> coordinates)
{
    cout << "Laser wird auf Position gefahren ohne zu Zeichnen!\n";
    lsr->addCoordinatesToDrawPlate(coordinates);

    lsr->invokeStateOffIndexToDrawPlate();
    // Delete nicht da im State geblieben wird sonst NULL Pointer Exception
}
