#include "drawop.h"
#include <QPainter>
#include <QPaintEvent>

DrawOP::DrawOP(QWidget *parent) : QWidget(parent)
{

}


void DrawOP::paintEvent(QPaintEvent*)
{

    QPainter qPaint(this);
    QPen qPaint_Pen (Qt::black,2, Qt::SolidLine);
    qPaint.setPen(qPaint_Pen);


    if(coordinates.size() > 1)
        for(int i = 0; i < coordinates.size() - 1; i++)
        {
            if(!jumpOfStateOff(&i)) // Ueberpreufung ob paint zwischen LaserOn/LaserOff
                qPaint.drawLine(coordinates[i][0], coordinates[i][1],
                                coordinates[i+1][0], coordinates[i+1][1]);
            else
            {
                qPaint.drawLine(coordinates[i-1][0], coordinates[i-1][1],
                                coordinates[i][0], coordinates[i][1]);
            }
        }
}

// Hinzufuegen der Koordinaten in den Vektor
void DrawOP::addCoordinatesToVector(vector<int> cord)
{
    coordinates.push_back(cord);
}

void DrawOP::setJumpIndex()
{
    indexStateOff.push_back(coordinates.size() - 1);
    // Vergleich ob Zwei Werte die Nebeneinander im Vector gespeichert sind, ++ sind
    if((indexStateOff[indexStateOff.size() - 1] - indexStateOff[indexStateOff.size() - 2]) == 1)
    {

        indexStateOff.erase(indexStateOff.begin() + indexStateOff.size() - 2); // Loesche
        indexStateOff[indexStateOff.size() -1 ] -= 1;                          // Reduziere Wert um 1
        coordinates.erase(coordinates.begin() + coordinates.size() - 1);       // Loesche Koordinaten
    }

}

void DrawOP::resetDrawPlate()
{
    coordinates.clear();
    indexStateOff.clear();
    this->update();
}

// Methode zur Ueberpruefung ob der Index im Paitevent
// den Abschnitt des Springens erreicht hat
bool DrawOP::jumpOfStateOff(int* i)
{
    if(indexStateOff.size() >0)
    {
        for(vector<int>::iterator iter = indexStateOff.begin(); iter != indexStateOff.end(); iter++)
            if(*i == *iter - 1)
                return true;
    }
    else
    {
        return false;
    }
    return false; // Warning vermeiden
}
