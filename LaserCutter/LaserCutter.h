#ifndef LASERCUTTER_H
#define LASERCUTTER_H

#include "Parser.h"


using std::string;
using std::iterator;


class LaserCutter
{
public:
    enum LaserStateOnGUI {OFF, ON, LASERN, ERROR};

    class LaserState* curState;

    /* Konstruktoren und Destruktor */
    LaserCutter();
    LaserCutter(string *);
    ~LaserCutter();

    /* Controller Methoden */
    void execOPCode(int drawTime);
    void setCurrentState(LaserState*);
    void assignLsrCGUI(class LaserCutterGUI* lsrcGUI);
    void abortDrawing();

    /* Methoden zur Kommunikation zur GUI */
    void addCoordinatesToDrawPlate(vector<int>);
    void setLaserCutterStateOnGUI(LaserStateOnGUI);
    void invokeStateOffIndexToDrawPlate();
    void msgToGUI(string*);

    /* State Methoden */
    void on();
    void off();
    void lasern(vector<int>);
    void error(std::string* msg);


private:
    // Referenzen des MVC
    Parser* par;
    class LaserCutterGUI* lsrcGUI;

    bool abort = false;

};

#endif // LASERCUTTER_H
