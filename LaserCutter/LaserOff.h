#ifndef LASEROFF_H
#define LASEROFF_H
#include "LaserState.h"

class LaserOff : public LaserState
{
public:
    LaserOff();
    ~LaserOff();

    void on(LaserCutter*);
    void move(LaserCutter*, vector<int>);

};

#endif // LASEROFF_H
