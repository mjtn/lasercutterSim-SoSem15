#include "LaserCutter.h"
#include "LaserOff.h"
#include "lasercuttergui.h"

#include <string>
#include <QTime>
#include <iterator>

using std::vector;
using std::distance;

// Konstruktoren und Destruktor
LaserCutter::LaserCutter()
{
}

LaserCutter::LaserCutter(string* path)
{
    // Parser initialiserung mit Pfad zur OpCode Datei
    par = new Parser(*path, this);
    // Einlesen der Codes aus der File
    par->readOPCodeFromFile();
    // Initial State = off
    setCurrentState(new LaserOff);

    //curState = new LaserOff();

}

LaserCutter::~LaserCutter()
{
    if(par != NULL)
        delete par;

}

// Methoden
/* Ausfuehren des Schneide Prozesses */
void LaserCutter::execOPCode(int drawTime)
{
    QTime tim;
    int x=0, y=0;

    // Deaktivieren des Start Buttons
    lsrcGUI->toggleBtnStatus(false);

    // Durchlaufen der OP-Codes
    for(vector<string>::iterator iter = par->opCodes.begin(); iter != par->opCodes.end(); iter++)
    {
        if(!this->abort)
        {
            // Koordinaten holen
            par->getCoordinates(distance(par->opCodes.begin(), iter), &x, &y);
            // Uebergabe der Koordinaten in die State Swtich Methode im Parser
            par->switchStates(&par->opCodes[distance(par->opCodes.begin(), iter)], x, y);

            // GUI zum Refresh zwingen
            lsrcGUI->update();

            // CurTime + Offset -> Verzoegerung
            tim = QTime::currentTime().addMSecs(drawTime);

            while(QTime::currentTime() < tim)
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        }
        else
        {
            // Bei Abbruch
            off();
            iter = par->opCodes.end() - 1;
            abort = false;
        }
    }
    lsrcGUI->toggleBtnStatus(true);

}

// Methoden zur Kommunikation mit der GUI

/* Koordinaten durchreichen an DrawPlate */
void LaserCutter::addCoordinatesToDrawPlate(vector<int> coords)
{
    if(!coords.empty())
        lsrcGUI->addCoordinatesToDrawPlate(coords);

}

/* State Anzeige auf der GUI aendern */
void LaserCutter::setLaserCutterStateOnGUI(LaserStateOnGUI lsrState)
{
    switch (lsrState) {
    case OFF:
        lsrcGUI->setLaserCutterStateOnUI("LASER OFF", Qt::darkRed);
        break;

    case ON:
        lsrcGUI->setLaserCutterStateOnUI("LASER ON", Qt::darkGreen);
        break;

    case LASERN:
        lsrcGUI->setLaserCutterStateOnUI("LASERT", Qt::darkCyan);
        break;

    case ERROR:
        lsrcGUI->setLaserCutterStateOnUI("LASER ERROR", Qt::darkRed);
        break;

    default:
        break;
    }
}

/* Index fuer das Bewegen ohne Zeichnen setzen */
void LaserCutter::invokeStateOffIndexToDrawPlate()
{
    lsrcGUI->setIndexOfStateOff();
}

/* Message bei Fehler an den User */
void LaserCutter::msgToGUI(std::string* msg)
{
    lsrcGUI->msgBoxShowWithErrorNotification(msg);
}


// Methoden zum State Switching
void LaserCutter::on()
{
    curState->on(this);
}

void LaserCutter::off()
{
    curState->off(this);
}

void LaserCutter::lasern(vector<int> coordinatesForMove)
{
    curState->move(this, coordinatesForMove);
}

void LaserCutter::error(string* msg)
{
    curState->error(this);
    msgToGUI(msg);
}

// Umschalten der States
void LaserCutter::setCurrentState(LaserState* newState)
{
    curState = newState;
}

/* Pointer der GUI zuweisen */
void LaserCutter::assignLsrCGUI(LaserCutterGUI* lsrcGUI)
{
    this->lsrcGUI = lsrcGUI;
}

// Abort des Zeichnens durch Button Event in der GUI
void LaserCutter::abortDrawing()
{
    abort = true;
}
