#ifndef LASERON_H
#define LASERON_H
#include "LaserState.h"


class LaserOn : public LaserState
{
public:
    LaserOn();
    ~LaserOn();

    void off(LaserCutter*);
    void move(LaserCutter*, vector<int>);

};

#endif // LASERON_H
