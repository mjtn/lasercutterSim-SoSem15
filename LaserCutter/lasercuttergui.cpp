#include "lasercuttergui.h"
#include "QFileDialog"
#include "QMessageBox"


LaserCutterGUI::LaserCutterGUI(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::LaserCutterGUI)
{
    ui->setupUi(this);
    setLaserCutterStateOnUI("LASER OFF", Qt::darkRed);
    // Signal Connect fuer Exit der Application
    connect(this->ui->btnClose, SIGNAL(clicked(bool)), qApp, SLOT(quit()));
}

LaserCutterGUI::~LaserCutterGUI()
{
    delete ui;
}

// Methoden
// Koordinaten zum Zeichnen in das Widget pushen
void LaserCutterGUI::addCoordinatesToDrawPlate(vector<int> coord)
{
    ui->wdgPlate->addCoordinatesToVector(coord);
}

// Anzeige des States
void LaserCutterGUI::setLaserCutterStateOnUI(string txt, Qt::GlobalColor color)
{
    ui->lblLaserState->setText(QString::fromStdString(txt));
    QPalette stylePal = ui->lblLaserState->palette();
    stylePal.setColor(ui->lblLaserState->foregroundRole(), color);
    ui->lblLaserState->setPalette(stylePal);
}

// Toggle des Buttons Start
void LaserCutterGUI::toggleBtnStatus(bool state)
{
    ui->btnStart->setEnabled(state);
}


// Reset des LaserCutters
void LaserCutterGUI::resetLsrCutter()
{
    ui->wdgPlate->resetDrawPlate();
    delete lsrCutter;
    this->setLaserCutterStateOnUI("LASER OFF", Qt::darkRed);
}

// Setzen des Indizes der als Uebergang von Zeichen gilt
void LaserCutterGUI::setIndexOfStateOff()
{
    ui->wdgPlate->setJumpIndex();
}

// Message an den User falls Error State Aktiv wird
void LaserCutterGUI::msgBoxShowWithErrorNotification(std::string* message)
{
    QMessageBox qMsg;

    QString msg = "Fehler!\n"+ QString::fromStdString(*message);
    qMsg.setIcon(QMessageBox::Information);

    qMsg.setText(msg);
    qMsg.exec();
}

// Slots
// File Browser for OPCodes
void LaserCutterGUI::on_btnPath_clicked()
{
    // File Browser Oeffnen und Pfad zur Datei erhalten
    QString filename = QFileDialog::getOpenFileName(this, "Search Path to OP Code", "OP Code File", tr("Text Files(*.txt)"));

    // Pfad der Datei -> Textbox
     ui->txtbPath->setText(filename);

}

// Start des Schneideprozesses
void LaserCutterGUI::on_btnStart_clicked()
{
    string path;

    // Falls Prozess erneut gestartet wird reset auf anfang
    resetLsrCutter();

    // Ueberpruefung auf leeren String
    if(!ui->txtbPath->text().toStdString().empty())
    {
        // Ausfuehren des Zeichenvorgangs
        path = ui->txtbPath->text().toStdString();

        lsrCutter = new LaserCutter(&path);
        lsrCutter->assignLsrCGUI(this);

        // Methode fuer das Zeichnen mit Zeit
        lsrCutter->execOPCode(500);
    }
    else
    {
        // Fehlermeldung in Form einer MsgBox
        QMessageBox qmsg;
        qmsg.setIcon(QMessageBox::Critical);
        qmsg.setText("Bitte wählen sie eine OP-Code Datei aus!");
        qmsg.exec();
    }

}

// LaserCutter reset to init
void LaserCutterGUI::on_btnStop_clicked()
{
    //resetLsrCutter();
    if(lsrCutter != NULL)
        lsrCutter->abortDrawing();
}

// Anwendung schliessen
void LaserCutterGUI::on_btnClose_clicked()
{
  // Verbunden durch Signal oben (s.h. Konstruktor)
}

