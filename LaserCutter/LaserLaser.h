#ifndef LASERLASER_H
#define LASERLASER_H
#include "LaserState.h"

class LaserLaser : public LaserState
{
public:
    LaserLaser();


    void on(LaserCutter *);
    void off(LaserCutter *);
    void move(LaserCutter*, vector<int>);

};

#endif // LASERLASER_H
