#include "LaserLaser.h"
#include "LaserOn.h"
#include "LaserOff.h"

LaserLaser::LaserLaser()
{

}


void LaserLaser::off(LaserCutter* lsr)
{
    cout << "Laser wird umgeschaltet in LaserLaser => NewState = off()\n";
    lsr->setCurrentState(new LaserOff());

    //lsr->lsrcGUI->setLaserCutterStateOnUI("LASER OFF", Qt::darkRed);
    lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::OFF);
    delete this;
}

void LaserLaser::on(LaserCutter* lsr)
{
    cout << "Laser wird umgeschaltet in LaserLaser => NewState = on()\n";
    lsr->setCurrentState(new LaserOn());
    lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::ON);
    delete this;
}

void LaserLaser::move(LaserCutter* lsr, vector<int> coordinates)
{

    lsr->addCoordinatesToDrawPlate(coordinates);
    cout << "Laser verbleibt in LaserLaser im State Lasern()\n";

}
