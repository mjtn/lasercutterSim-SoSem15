#include "Parser.h"
#include "LaserCutter.h"

Parser::Parser(string path, LaserCutter* lsr)
{
	this->path = path;
    this->lsr = lsr;
    initDispatch();
}

Parser::~Parser()
{
}

// Einlesen des OP Codes aus der File
void Parser::readOPCodeFromFile()
{
	std::string puffer; // Puffer fuer das einlesen
	this->fi.open(this->path, std::ios::in); // Oeffne die Datei mit Path nur einlesend
	if (this->fi.good()) // Falls die Datei vorhanden ist 
		while (!this->fi.eof()) // lese solange wie das EOF nicht erreicht ist !
		{
			getline(fi, puffer); // Lese eine Zeile und puffere
            if (!(puffer.find("#") != std::string::npos || puffer.length() == 0)) // Ignoriere Zeilen die mit # Anfangen (oder eines Enthalten) || Leerzeile
                this->opCodes.push_back(puffer); // Schiebe den inhalt des Puffers an das Ende des Vector
			
		}
    else
    {
        string errorMsg = path + "\nDatei konnte nicht gefunden werden!";
        lsr->msgToGUI(&errorMsg);
    }
	this->fi.close(); // Schiesse die datei anschliesend wieder
}

// Uebergibt an die Adressen der beiden ints die Koordinaten x, y
void Parser::getCoordinates(int posInVec, int* x, int* y)
{
    if (this->validOPCode(&this->opCodes[posInVec]))
	{
        std::size_t pos = this->opCodes[posInVec].find(", ");
		if (pos == std::string::npos) // Falls kein , im String gefunden worden ist --> LASER ON / LASER OFF
		{
			*x = -1; *y = -1;
		}
		else
        {
            try // Abfangen von Fehlerhaften OP-Code Files
            {
            // Position des ersten Whitespaces ermitteln + 1 (Damit es nicht in die fktn uebergeben wird)
            *x = std::stoi(this->opCodes[posInVec].substr(this->opCodes[posInVec].find(" ") + 1));//, this->op_Codes[pos_in_vec].find(" ") + 1));
			/* Substring mit der pos (s.h. oben) MOVE 10, 10
														  ^^
			*/
            *y = std::stoi(this->opCodes[posInVec].substr(pos + 1, this->opCodes[posInVec].find("\n")));
            }catch(std::invalid_argument)
            {
                *x = -1;
                *y = -1;
            }
		}
	}
	else
	{
        *x = -1; *y = -1;
	}
	
}

// Wechsel der States und durchreichen der OP-Codes
void Parser::switchStates(string* opCode, int x, int y)
{
    string errorMsg;
    if(validOPCode(opCode))
    {
        if(*opCode == "LASER ON")
            lsr->on();
        else if (*opCode == "LASER OFF")
            lsr->off();
        else if( x > -1 && y > -1)
        {
            vector <int> tmpCoordinates;
            tmpCoordinates.push_back(x);
            tmpCoordinates.push_back(y);
            std::cout << "Koordinaten zum Zeichnen: X: " << x << " Y: " << y << "\n";
            lsr->lasern(tmpCoordinates);

        }
        else
        {
            errorMsg = *opCode + "\nist kein Gültiger OP-Code!";
           lsr->error(&errorMsg);
        }
    }else
    {
        errorMsg = *opCode + "\nist kein Gültiger OP-Code!";
        lsr->error(&errorMsg);
    }
}

// Ueberpruefung des OP Codes 
bool Parser::validOPCode(string* opcode)
{
    // Durchsuche den Pointer nach den OP-Codes die Valid sind s.h. init_dispatch();
    for(vector<string>::iterator iter = dispatchTable.begin(); iter != dispatchTable.end(); iter++)
        if(opcode->find(*iter) != string::npos) // Falls einer der validen OP-Codes zutrifft
            return true;        // --> OP-Code is Valid

    return false;               // Falls nicht --> Invalid
}

// Dispatch Table init mit gueltigen OP Code Befehlen
void Parser::initDispatch()
{
    this->dispatchTable.push_back("LASER ON");
    this->dispatchTable.push_back("LASER OFF");
    this->dispatchTable.push_back("MOVE");

}
