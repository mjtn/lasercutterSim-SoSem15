#include "LaserOn.h"
#include "LaserState.h"
#include "LaserOff.h"
#include "LaserLaser.h"
LaserOn::LaserOn()
{

}
LaserOn::~LaserOn()
{

}

void LaserOn::off(LaserCutter* lsr)
{
    cout << "Laser wird umgeschaltet in LaserOn=>NewState = Off()\n";
    lsr->setCurrentState(new LaserOff());
    lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::OFF);
    delete this;
}

void LaserOn::move(LaserCutter* lsr, vector<int> coordinates)
{
    cout << "Laser wird umgeschaltet in LaserOn=>NewState = lasern()\n";
    lsr->setCurrentState(new LaserLaser());
    lsr->addCoordinatesToDrawPlate(coordinates);
    lsr->setLaserCutterStateOnGUI(LaserCutter::LaserStateOnGUI::LASERN);

    //lsr->lsrcGUI->setLaserCutterStateOnUI("LASERT", Qt::darkCyan);
    delete this;
}

// LaserOn::on wird nicht benoetigt da in der Oberklasse die
// Funktion definiert ist.
